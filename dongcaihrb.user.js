// ==UserScript==
// @name         东财会计继续教育看课自动
// @namespace    http://tampermonkey.net/
// @version      0.1
// @author       hlkty86
// @include      *://training.hebkj.edufe.cn/course/video/*
// ==/UserScript==
(function() {
    detect();
    function detect(){
        //检查一次是否有弹窗
        if(document.querySelector(".pv-ask-modal")){
            //点击弹窗的跳过按钮
            document.querySelector(".pv-ask-skip").click();
            //10秒循环
             setTimeout(detect,10000);
        }
        else{
            //10秒循环
            setTimeout(detect,10000);
        }
    }
})();